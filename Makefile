PROJECT = skeleton
CC = arm-none-eabi-gcc
OBJCOPY = arm-none-eabi-objcopy
GDB = arm-none-eabi-gdb

# Flags for debugging, warnings and architecture
CFLAGS = -g -O2 -Wall
CFLAGS += -mlittle-endian -mthumb -mcpu=cortex-m4 -mthumb-interwork
CFLAGS += -mfloat-abi=soft -mfpu=fpv4-sp-d16
CFLAGS += --specs=nosys.specs

# Our Top Level Source and Header files
SRCS = src/*.c
SRCS += src/*.s
CFLAGS += -Iinc

# HAL Source and Header files
HAL_SRCS = drivers/STM32F4xx_HAL_Driver/Src
SRCS += $(HAL_SRCS)/stm32f4xx_hal.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_dma.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_flash.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_flash_ex.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_gpio.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_i2c.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_pwr.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_pwr_ex.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_rcc.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_cortex.c
SRCS += $(HAL_SRCS)/stm32f4xx_hal_spi.c
CFLAGS += -Idrivers/STM32F4xx_HAL_Driver/Inc

# BSP Source and Header files
SRCS += drivers/BSP/STM32F429I-Discovery/stm32f429i_discovery.c
CFLAGS += -Idrivers/BSP/STM32F429I-Discovery

#CMSIS Source and Header files
CFLAGS += -Idrivers/CMSIS/Device/ST/STM32F4xx/Include
CFLAGS += -Idrivers/CMSIS/Include

# Linker Script
CFLAGS += -Tlinkerscripts/STM32F429ZITx_FLASH.ld

all: $(PROJECT).bin

# copy the .elf binary to a .bin format so we can upload it to the board.
$(PROJECT).bin: $(PROJECT).elf
	$(OBJCOPY) -O binary $(PROJECT).elf $(PROJECT).bin

# compile to the .elf binary
$(PROJECT).elf: $(SRCS)
	$(CC) $(CFLAGS) $^ -o $@

# upload the binary to the board (flash memory starts in 0x08000000)
flash: $(PROJECT).bin
	st-flash write $(PROJECT).bin 0x08000000

start-debug-server:
	st-util &

kill-debug-server:
	pkill st-util

# This command uses arm-none-eabi-gdb with Data Display Debugger.
# It runs the debugger on our .elf binary and execute commands from the debugscript file.
# This file contains commands to connect to the debug server, load the .elf file on the board and then open the main.c file in the debugger window.
# You can change this if you don't want to use a graphical front end for the debugger.
# Before we debug, we need to run start-debug-server and after it run kill-debug-server.
debug: $(PROJECT).elf
	ddd --debugger $(GDB) --command debugscript $(PROJECT).elf

clean:
	rm -f $(PROJECT).elf $(PROJECT).bin

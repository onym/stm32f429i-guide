#!/bin/bash
#---------------------------------------------------------------------------
# Test for a directory argument
stm32cubefolder=./STM32CubeF4-master
if [ -d "$1" ]
then
    stm32cubefolder=$1
fi
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
# Copy files from the firmware package
mkdir src
mkdir inc
mkdir linkerscripts
cp -r $stm32cubefolder/Drivers/ ./drivers
cp -r $stm32cubefolder/Projects/STM32F429I-Discovery/Templates/Src/. ./src/
cp $stm32cubefolder/Projects/STM32F429I-Discovery/Templates/SW4STM32/startup_stm32f429xx.s ./src/
cp -r $stm32cubefolder/Projects/STM32F429I-Discovery/Templates/Inc/. ./inc/
cp $stm32cubefolder/Projects/STM32F429I-Discovery/Templates/SW4STM32/STM32F429I_DISCO/STM32F429ZITx_FLASH.ld ./linkerscripts/
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
# Commenting all unnecessary module inclusions in ./inc/stm32f4xx_hal_conf.h:
# List of modules we want to use
modules="DMA FLASH GPIO I2C PWR RCC SPI CORTEX"
# Format the list for our regex
formatted_modules=$(echo $modules | sed 's/ /_\\|/g; s/$/_/')
# 1. Find all lines which are not #define HAL_(one of our modules)_MODULE_ENABLED.
# 2. Out of those lines, comment those which start with #define HAL_.
# I use this algorithm because lookahead is not supported in sed.
sed -i "/^#define HAL_\($formatted_modules\)\?MODULE_ENABLED$/! s/^#define HAL_/\/\/ #define HAL_/" ./inc/stm32f4xx_hal_conf.h
#---------------------------------------------------------------------------

#---------------------------------------------------------------------------
# Uncomment the stm32f429 module inclusion in the stm32f4xx.h cmsis driver file
sed -i "s/\/\* #define STM32F429xx \*\//#define STM32F429xx/" ./drivers/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h
#---------------------------------------------------------------------------
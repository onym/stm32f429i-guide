# Introduction
The following guide will help you to:
* Create a complete toolchain for programming stm32f4 boards. the toolchain will not be bound to any IDE.
* Create a skeleton for projects for the stm32f429i board.

This guide is targeted at linux users.

# Software You Need
* STM32CubeF4 MCU Firmware Package
* GNU Arm Embedded Toolchain
* stlink
* Data Display Debugger (read the debug command in the makefile for more information)

## STM32CubeF4 MCU Firmware Package
This package contains all files necessary for us to build an stm32f4 project.
Read more about it [here](https://www.st.com/resource/en/user_manual/dm00107720-getting-started-with-stm32cubef4-mcu-package-for-stm32f4-series-stmicroelectronics.pdf).

[Project link](https://github.com/STMicroelectronics/STM32CubeF4)

## GNU Arm Embedded Toolchain
This is the toolchain we will use to build a binary from our files. It includes several open source GNU tools for Arm processors: GCC, GDB, binutils and libraries.
If you are on ubuntu you might run into some issues  trying to install this toolchain. Follow the steps in this [post](https://askubuntu.com/questions/1243252/how-to-install-arm-none-eabi-gdb-on-ubuntu-20-04-lts-focal-fossa/1243405#1243405) to install it. Remember - the binaries we need for this toolchain are arm-none-eabi-gcc, arm-none-eabi-gdb and arm-none-eabi-objcopy.

[Project link](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)

## stlink
This is an open source STM32 MCU programming toolset. We will use this program to flash our binaries to our board and start a debug server to debug them.

[Project link](https://github.com/stlink-org/stlink)

# How to use the shell script
This script takes as an argument the STM32CubeF4 folder path. For example: ```./script.sh /home/username/Downloads/STM32CubeF4-master```
If no argument is supplied the script would assume that the path is ./STM32CubeF4-master.
The shell script creates several directories in your project folder and copies files from the firmware package to them.
It will generate the following project structure:
* drivers/
    * BSP/
    * CMSIS/
    * STM32F4xx_HAL_Driver/
* inc/
    * main.h
    * stm32f4xx_hal_conf.h
    * stm32f4xx_it.h
* src/
    * main.c
    * startup_stm32f429xx.c
    * stm32f4xx_hal_msp.c
    * stm32f4xx_it.c
    * system_stm32f4xx.c
* linkerscripts/
    * STM32F429ZITx_FLASH.ld

After that it edits two files:
1. ./inc/stm32f4xx_hal_conf.h - This file defines our configuration for the HAL library files. It includes the header files of the  HAL module files we want to compile in our binary. We need to comment out all unnecessary module inclusions. The modules this scripts does not comment are the ones necessary for GPIO programming, a basic function needed for most projects i assumed.
2. ./drivers/CMSIS/Device/ST/STM32F4xx/Include/stm32f4xx.h - This file contains a configuration section that allows to select the STM32F4xx device used in the target application. We need to uncomment the line for our device (stm32f429i).

# The Makefile
Please read through the Makefile to understand it. Here are its commands:
* ```make``` - create binary
* ```make flash``` - flash binary to board
* ```make start-debug-server``` - start the debug server
* ```make debug``` - run the debugger
* ```make kill-debug-server``` - stop the debug server
* ```make clean``` - remove compiled files

# Recommended Documentation
* [Datasheet](https://www.st.com/resource/en/datasheet/dm00071990.pdf) - Contains electrical information
* [Reference Manual](https://www.st.com/resource/en/reference_manual/dm00031020-stm32f405-415-stm32f407-417-stm32f427-437-and-stm32f429-439-advanced-arm-based-32-bit-mcus-stmicroelectronics.pdf) - Contains register information
* [Description of STM32F4 HAL and low-layer drivers](https://www.st.com/resource/en/user_manual/dm00105879-description-of-stm32f4-hal-and-ll-drivers-stmicroelectronics.pdf) - Contains HAL API information

# What's Next
* You can read and analyze the code in the examples from the STM32CubeF4 package.
* You can watch the [Modern Embedded Systems Programming Course](https://www.youtube.com/playlist?list=PLPW8O6W-1chwyTzI3BHwBLbGQoPFxPAPM).
